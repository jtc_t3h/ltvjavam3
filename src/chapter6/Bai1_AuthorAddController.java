package chapter6;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AuthorDAO;
import domain.Author;

@WebServlet("/chapter6/author/add.html")
public class Bai1_AuthorAddController extends HttpServlet {

	private AuthorDAO repository = new AuthorDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/chapter6/bai1_author_add.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Author obj = new Author(null, request.getParameter("name"));
		
		repository.insert(obj);
		response.sendRedirect(request.getContextPath() + "/chapter6/author/list.html");
	}
}
