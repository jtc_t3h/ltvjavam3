package chapter6;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AuthorDAO;
import domain.Author;

/**
 * Servlet implementation class Bai1_AuthorEditController
 */
@WebServlet("/chapter6/author/edit.html")
public class Bai1_AuthorEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai1_AuthorEditController() {
		super();
		// TODO Auto-generated constructor stub
	}

	AuthorDAO repository = new AuthorDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		request.setAttribute("o", repository.findById(id));
		
		request.getRequestDispatcher("/chapter6/bai1_author_edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Author obj = new Author(id, request.getParameter("name"));
		
		repository.update(obj);
		response.sendRedirect(request.getContextPath() + "/chapter6/author/list.html");
	}

}
