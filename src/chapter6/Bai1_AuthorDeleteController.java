package chapter6;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AuthorDAO;

/**
 * Servlet implementation class Bai1_AuthorDeleteController
 */
@WebServlet("/chapter6/author/del.html")
public class Bai1_AuthorDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	private AuthorDAO authorDAO = new AuthorDAO();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bai1_AuthorDeleteController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sId = request.getParameter("id");
		Integer id = Integer.parseInt(sId);
		
		authorDAO.delete(id);
		
		response.sendRedirect("list.html");
	}

}
