package chapter6;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AuthorDAO;
import domain.Author;

/**
 * Servlet implementation class Bai1_AuthorController
 */
@WebServlet("/chapter6/author/list.html")
public class Bai1_AuthorController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private AuthorDAO authorDAO = new AuthorDAO();   
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bai1_AuthorController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Author> list = authorDAO.findAll();
		request.setAttribute("list", list);
		request.getRequestDispatcher("/chapter6/bai1_author_list.jsp").forward(request, response);
	}

	

}
