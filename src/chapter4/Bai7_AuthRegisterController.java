package chapter4;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MemberDAO;
import domain.Member;

/**
 * Servlet implementation class Bai7_AuthRegisterController
 */
@WebServlet("/chapter4/bai7_auth_register.html")
public class Bai7_AuthRegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private MemberDAO memberDAO = new MemberDAO();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bai7_AuthRegisterController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/chapter4/bai7_auth_register.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String fullname = request.getParameter("fullName");
		String email = request.getParameter("email");
		String username = request.getParameter("usr");
		String password = request.getParameter("pwd");
		byte gender = Byte.parseByte(request.getParameter("gender") );
		
		Member member = new Member();
		member.setUsername(username);
		member.setPassword(password);
		member.setEmail(email);
		member.setGender(gender);
		member.setFullname(fullname);
		member.setModifiedDate(new Date(System.currentTimeMillis()));
		member.setAddedDate(new Date(System.currentTimeMillis()));
		
		if (memberDAO.add(member) > 0) {
			response.sendRedirect(request.getContextPath() + "/auth/logon.html");
		} else {
			request.setAttribute("msg", "Register Failed");
			doGet(request, response);
		}
	}

}
