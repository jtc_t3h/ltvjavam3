package chapter4;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PublisherRepository;

/**
 * Servlet implementation class Bai1_PublisherController
 */
@WebServlet("/chapter4/bai1-publisher-list.html")
public class Bai1_PublisherController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai1_PublisherController() {
		super();
		// TODO Auto-generated constructor stub
	}

	private PublisherRepository repository = new PublisherRepository();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setAttribute("list", repository.getPublishers());
			request.getRequestDispatcher("/chapter4/bai1_publisher_list.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
