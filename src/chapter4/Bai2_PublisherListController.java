package chapter4;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PublisherRepository;
import domain.Publisher;

/**
 * Servlet implementation class Bai2_PublisherListController
 */
@WebServlet("/chapter4/bai2-publisher-list.html")
public class Bai2_PublisherListController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PublisherRepository repository = new PublisherRepository();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai2_PublisherListController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			// Lay du lieu tu DAO
			List<Publisher> listPublishers = repository.getPublishers();

			// Gui du lieu ra trang JSP
			request.setAttribute("list", listPublishers);

			// Chuyen trang dung request dispatcher
			request.getRequestDispatcher("/chapter4/bai2_publisher_list.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
