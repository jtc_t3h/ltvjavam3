package dao;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.Transaction;

import domain.Member;
import domain.Publisher;

public class MemberDAO extends Repository {

	public int add(Member member) {
		member.setId(random());
		member.setPassword(sha256(member.getPassword()).toString());

		Session session = HibernateUtils.getSessionFactory().openSession();
		try{
			Transaction trans = session.beginTransaction();
			session.save(member);
			trans.commit();
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	private static long random() {
		Random rand = new Random();
		long a = rand.nextInt();
		long b = rand.nextInt();
		return a * b;
	}

	private static byte[] sha256(String plaintext) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");

			return digest.digest(plaintext.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
			return null;
		}
	}
	
	// Dung PP JDBC
	public Member findByUserName(String username) throws SQLException {
		Member member = null;
		try {
			open();
			
			stmt = connection.createStatement();
			rs = stmt.executeQuery("SELECT * FROM member where username = '" + username + "'");
			
			while (rs.next()) {
				member = new Member();

				member.setId(rs.getLong(1));
				member.setUsername(rs.getString(2));
				member.setPassword(rs.getString(3));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			close();
		}
		return member;
	}
}
