package dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import domain.Author;

public class AuthorDAO {

	public List<Author> findAll(){
		Session session = HibernateUtils.getSessionFactory().openSession();
		
		Query query = session.createQuery("from Author");
		return query.getResultList();
	}
	
	public Author findById(int id){
		Session session = HibernateUtils.getSessionFactory().openSession();
		
		return session.find(Author.class, id);
	}
	
	public Integer insert(Author author){
		Transaction tran = null;
		try (Session session = HibernateUtils.getSessionFactory().openSession()){
			tran = session.beginTransaction();
			session.save(author);
			tran.commit();
			return 1;
		} catch (Exception e) {
			if(tran != null) {
			tran.rollback();
			}
			return 0;
		}
	}
	
	public boolean update(Author author) {
		Transaction tran = null;
		try (Session session = HibernateUtils.getSessionFactory().openSession()){
			tran = session.beginTransaction();
			session.merge(author);
			tran.commit();
			return true;
		} catch (Exception e) {
			if(tran != null) {
			tran.rollback();
			}
		}
		
		return false;
	}

	public void delete(Integer id) {
		
		Transaction trans = null;
		try (Session session = HibernateUtils.getSessionFactory().openSession()){
			trans = session.beginTransaction();
			
			Author author = session.find(Author.class, id);
			session.delete(author);
			
			trans.commit();
		} catch (Exception e) {
			if (trans != null) {
				trans.rollback();
			}
			e.printStackTrace();
		}
		
		
	}
}
