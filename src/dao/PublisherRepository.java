package dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import domain.Publisher;

public class PublisherRepository extends Repository {

	public List<Publisher> getPublishers() throws SQLException {
		try {
			open();
			stmt = connection.createStatement();
			rs = stmt.executeQuery("SELECT * FROM Publisher");
			
			List<Publisher> list = new LinkedList();
			while (rs.next()) {
				Publisher publisher = new Publisher();
				publisher.setId(rs.getLong("PublisherId"));
				publisher.setName(rs.getString("PublisherName"));

				list.add(publisher);
			}
			return list;
		} finally {
			close();
		}
	}
}
