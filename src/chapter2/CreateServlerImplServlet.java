package chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class CreateServlerImplServlet implements Servlet {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// Nghiệp vụ xử lý sẽ được servlet thực hiện trong hàm service
		// Servlet xử lý trả về 1 chuỗi theo định dạng html
		
		PrintWriter in = res.getWriter();
		in.write("<html>");
		in.write("<body>");
		
		in.write("<h1>Hello world.</h1><br>");
		in.write("<h3>Create servlet by implement Servlet.</h3>");
		
		in.write("</body>");
		in.write("</html>");
		
	}

}
