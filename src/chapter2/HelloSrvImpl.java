package chapter2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class HelloSrvImpl implements Servlet {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		ServletContext context = config.getServletContext();
		
		Enumeration<String> paramNames = context.getAttributeNames();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();;
			String paramValue = context.getInitParameter(paramName);
		}
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		System.out.println("HelloSrvImpl");
		
		ServletContext context = getServletConfig().getServletContext();
		Enumeration<String> paramNames = context.getAttributeNames();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();;
			String paramValue = context.getInitParameter(paramName);
		}
		
		PrintWriter out = res.getWriter();
		out.print("<h1> Hello world by Implement Servlet");
	}

}
