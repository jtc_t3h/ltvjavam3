package chapter2;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MemberDAO;
import domain.Member;

/**
 * Servlet implementation class LoginServletController
 */
@WebServlet("/chapter2/login-mvc.html")
public class LoginServletController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private MemberDAO memberDAO = new MemberDAO();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServletController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String username = request.getParameter("usr");
		String password = request.getParameter("pwd");
		
		try {
			Member member = memberDAO.findByUserName(username);
			if (password.equals(member.getPassword())) {
				
				// Tạo đối tượng HttpSession -> dùng đối tượng Request
				System.out.println("Login: session created.");
				HttpSession session = request.getSession();
				
				
				// Gửi dữ liệu cho trang welcome.jsp
				session.setAttribute("USER_NAME", username);
				
				response.sendRedirect("welcome.jsp");
			} else {
				request.setAttribute("message", "Invalid username or password.");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
