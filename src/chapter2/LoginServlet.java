package chapter2;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Kiem tra authentication (khong dung database)
 *   
 * @author hv
 *
 */
public class LoginServlet extends HttpServlet {

//	private ServletConfig config;
	
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		System.out.println("username =" + config.getInitParameter("username"));
		System.out.println("password =" + config.getInitParameter("password"));
		
//		this.config = config;
		super.init(config);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// Quet tat ca cac init-param
		ServletConfig config = getServletConfig();
	
		
		System.out.println(config + "===============");
		
		Enumeration<String> paraNames = config.getInitParameterNames();
		while (paraNames.hasMoreElements()) {
			String paraName = paraNames.nextElement();
			System.out.println(paraName +  " = " + config.getInitParameter(paraName));
		}
		
		System.out.println("======== Parameter ========");
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		System.out.println(username);
		System.out.println(password);
	}
	
	
}
