package chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreateServletByAnnotation
 */
@WebServlet("/chapter2/CreateServletByAnnotation")
public class CreateServletByAnnotation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServletByAnnotation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter in = response.getWriter();
		in.write("<html>");
		in.write("<body>");
		
		in.write("<h1>Hello world.</h1><br>");
		in.write("<h3>Create servlet by implement Servlet.</h3>");
		
		in.write("</body>");
		in.write("</html>");
		
	}

}
