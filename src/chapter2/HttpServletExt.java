package chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/chapter2/hello-ext2.html")
public class HttpServletExt extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		System.out.println(context.getInitParameter("adminEmail"));
		System.out.println(context.getInitParameter("adminNumber"));
		
		PrintWriter out = res.getWriter();
		out.print("<h1> Hello world by extend HttpServlet");
	}

}
