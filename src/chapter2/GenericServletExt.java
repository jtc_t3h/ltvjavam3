package chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class GenericServletExt extends GenericServlet {

	@Override
	public void init() throws ServletException {
		System.out.println("Init a instance of GenericServletExt");
	}
	
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		PrintWriter out = res.getWriter();
		out.print("<h1> Hello world by extend GenericServlet");
	}

}
