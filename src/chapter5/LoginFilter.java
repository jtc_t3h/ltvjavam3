package chapter5;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		Filter.super.destroy();
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		Filter.super.init(filterConfig);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		// Kiểm tra client đã đăng nhập hay chưa.
		// Nếu đã đăng nhập thì cho pass, ngược lại chuyển sang trang login page
		
		// Kiểm tra đăng nhập dựa vào biến USER_NAME của Session
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		HttpSession session = req.getSession();
		if (req.getRequestURI().endsWith("login-mvc.html") ||
				session.getAttribute("USER_NAME") != null) {
			chain.doFilter(request, response);
		} else {
			res.sendRedirect("/LTVJAVAM3/chapter2/login-mvc.html");
		}
		
	}

}
