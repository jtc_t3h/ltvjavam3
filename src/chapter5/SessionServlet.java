package chapter5;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SessionServlet
 */
@WebServlet("/chapter5/session.html")
public class SessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("username", request.getParameter("username"));
		
		String crudAction = request.getParameter("crudation");
		if ("chosen".equalsIgnoreCase(crudAction)) {
			String product = request.getParameter("product");
			
			 List<String> listOfProduct = (List<String>)session.getAttribute("listOfProduct");
			 if (listOfProduct == null) {
				 listOfProduct = new ArrayList<String>();
			 }
			 listOfProduct.add(product);
			 
			 session.setAttribute("listOfProduct", listOfProduct);
			 
			 // Gui message ra trang JSP -> Da them <<ten SP>> thanh cong. 
			 session.setAttribute("message", "Ban da chon SP: " + product + " thanh cong."); 
		} else if ("payment".equalsIgnoreCase(crudAction)) {
			session.removeAttribute("message");
			session.setAttribute("payment", "Ban da mua cac SP sau:");
		}
		
		response.sendRedirect("session.jsp");
	}

}
