<%@page import="chapter5.SessionCounterListener"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Session page demo</title>
</head>
<body>
  Session ID = <%= session.getId() %><br>
  Welcome, ${username }
  <hr>
  
  <c:choose>
    <c:when test="${message != null }">${message }</c:when>
    <c:when test="${payment != null }">
      <h2>${payment }</h2>
      <c:forEach var="sp" items="${listOfProduct }">
        ${sp }
      </c:forEach>
    </c:when> 
    <c:otherwise>Oh.No</c:otherwise>
  </c:choose>
  
  <hr>
  <h2>Total session: <%=SessionCounterListener.totalSession %></h2>
</body>
</html>