<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Module 03 LẬP TRÌNH WEB VỚI JAVA</title>
<link rel="stylesheet" type="text/css" href="/LTVJAVAM3/css/css.css">
</head>
<body>
  <div class="title">
    <div class="container">
      <div class="brand">
        <a href="/LTVJAVAM3">Module 3</a>
      </div>
    </div>
  </div>
  <div class="sidebar">
    <ul>
      <li><a href="helloworld.html">Hello World</a></li>
      <li><a href="sumtwonumbers.html">Sum Two Numbers</a></li>
      <li><a href="summultinumbers.html">Sum Multiple Numbers</a></li>
      <li><a href="register.html">Register</a></li>
      <li><a href="login.jsp">Login</a></li>
      <li><a href="multiplicationtable.jsp">Multiplication
          Table</a></li>
      <li><a href="upload.html">Upload Image</a></li>
      <li><a href="upload/multi.html">Multiple Upload Image</a></li>
      <li><a href="template.jsp">Template Example</a></li>
      <li><a href="admin/publisher.html">Publisher</a>
      <li><a href="admin/category.html">Category</a></li>
      <li><a href="home.html">Home</a></li>
      <li><a href="auth/register.html">Register</a></li>
      <li><a href="auth/logon.html">Log On</a></li>
      <li><a href="admin/invoice.html">Invoice</a></li>
    </ul>
  </div>
  <div class="main">
    <h2>Register</h2>
    <hr>
    <form method="post" class="form">
      <p><label>Username</label> <input type="text" name="usr"></p>
      <p><label>Password</label> <input type="password" name="pwd"></p>
      <p><label>Email</label> <input type="text" name="email"></p>
      <p>
        <label>Gender</label> <select name="gender">
          <option value="0">Male</option>
          <option value="1">Female</option>
          <option value="2">Undefined</option>
        </select>
      </p>
      <p>
        <button>Register</button>
      </p>
    </form>
    <c:if test="${'POST' eq pageContext.request.method }">
      <p>Username: ${param.usr }</p>
      <p>Password: ${param.pwd }</p>
      <p>Email: ${param.email }</p>
      <p>Gender: ${param.gender == '0'? 'Male': 'Female'}></p>
           
    </c:if>
    
    <%-- <% if (request.getMethod().equals("POST")) { %>
        <p>Username: ${param.usr }</p>
        <p>Password: ${param.pwd }</p>
        <p>Email: ${param.email }</p>
        <p>Gender:
           request.getParameter("gender")
           <% 
           if ("0".equals(request.getParameter("gender"))) { 
             out.println("Male");
           } else if ("1".equals(request.getParameter("gender"))) {
        	 out.println("Female");
           } else {
        	 out.println("Undefined");
           }
           %>
        </p>
    <% } %> --%>
  </div>
</body>
</html>