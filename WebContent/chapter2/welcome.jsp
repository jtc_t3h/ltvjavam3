<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Module 03 LẬP TRÌNH WEB VỚI JAVA</title>
<link rel="stylesheet" type="text/css" href="/LTVJAVAM3/css/css.css">
</head>
<body>
  <div class="title">
    <div class="container">
      <div class="brand">
        <a href="/LTVJAVAM3">Module 3</a>
      </div>
    </div>
  </div>
  <div class="sidebar">
    <ul>
      <li><a href="helloworld.html">Hello World</a></li>
      <li><a href="sumtwonumbers.html">Sum Two Numbers</a></li>
      <li><a href="summultinumbers.html">Sum Multiple Numbers</a></li>
      <li><a href="register.html">Register</a></li>
      <li><a href="login.jsp">Login</a></li>
      <li><a href="multiplicationtable.jsp">Multiplication
          Table</a></li>
      <li><a href="upload.html">Upload Image</a></li>
      <li><a href="upload/multi.html">Multiple Upload Image</a></li>
      <li><a href="template.jsp">Template Example</a></li>
      <li><a href="admin/publisher.html">Publisher</a>
      <li><a href="admin/category.html">Category</a></li>
      <li><a href="home.html">Home</a></li>
      <li><a href="auth/register.html">Register</a></li>
      <li><a href="auth/logon.html">Log On</a></li>
      <li><a href="admin/invoice.html">Invoice</a></li>
    </ul>
  </div>
  <div class="main">
    <h2>Welcome page</h2>
    <hr>
    <h3>Xin chào <%=session.getAttribute("USER_NAME") %></h3> <%-- JSP expression --%>
    <h3>Xin chào ${USER_NAME }</h3> <%-- dùng Expression Language --%>
    <h3><a href="logout.html">Đăng xuất</a></h3>
  </div>
</body>
</html>